include(src/OGTatt.pri)

TARGET = ogtatt

    LIBS += \
    $${PWD}/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++17 -O2

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

OTHER_FILES += \
    Docs/Ideas.txt

unix {
    isEmpty(DATADIR) {
        DATADIR = ~/.local/share
    }
    DEFINES += DATADIR=\\\"$${DATADIR}/ogtatt\\\"

    target.path = /usr/games/
    INSTALLS += target

    resources.path = $$DATADIR/luckey/ogtatt/
    resources.files = Resources/*
    INSTALLS += resources

    icon.path = $$DATADIR/icons/
    icon.files = ogtatt.svg
    INSTALLS += icon

    desktop.path = $$DATADIR/applications/
    desktop.files = ogtatt.desktop
    INSTALLS += desktop
}
