
![Original Gangster Tattoo](logo.png)

# Respect is only the beginning

[![pipeline status](https://gitlab.com/luckeyproductions/ogtatt/badges/master/pipeline.svg)](https://gitlab.com/luckeyproductions/ogtatt/-/commits/master) [![coverage report](https://gitlab.com/luckeyproductions/ogtatt/badges/master/coverage.svg)](https://gitlab.com/luckeyproductions/ogtatt/-/commits/master)

### Summary
Original Gangster Tattoo is a free and open source online top-down gangwar game being developed using the [Dry](https://gitlab.com/luckeyproductions/dry) game engine.

<!-- ### Installation
#### Linux

#### Compiling from source

You may try compiling by running this line in a terminal:

```
git clone https://github.com/LucKeyProductions/OGTatt; cd OGTatt; ./install.sh; cd ..; rm -rf OGTatt
``` -->

### Controls

#### Controller

Move | Fire
-----|------
Left stick | Right stick

#### Keyboard

Move | Fire
-----|------
WASD | Numpad

### Screenshot
![OG Tatt screenshot](Screenshots/Screenshot_Thu_Jun__2_02_08_15_2016.png)