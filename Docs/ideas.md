**OG Tatt** is a top-down urban shoot-em-up set in a world called Art where respect is only the beginning. 
Players can play without joining an existing gang. Play on your own and create a gang with at least three people whenever you feel like it. You'll have to earn enough respect to get an HQ and to be mentioned.  

# OG Tatt: Operation Stellar Rocket
_Expansion_

The Luciferian [cardinals](https://en.wikipedia.org/wiki/The_Spanish_Inquisition_(Monty_Python\)) activate a tricol portal to another dimension. Some [demons](https://en.wikipedia.org/wiki/Childhood's_End) sent an RSVP-landmine, inviting them to an apocalypse reveal party of some planet. After having a blast, the cardinals are provided with a warp-capable trinket and thus are the first gang on Art to go into space. Of course the AAA is furious with envy, Zebratools Inc. is very curious about this vessel and would like to reproduce it.
 
# Gameplay

If you decide to join a gang you can either fight other gangs in each city or smuggle warez to other HQs of your gang in other cities.

## Controls

* Keyboard
   - `Enter`: Enter/Leave nearest vehicle on the most convenient side
   - `]`: Specifically enter/leave nearest vehicle on the right side
   - `[`: Specifically enter/leave nearest vehicle on the left side
 
## UI

+ Ability to create animated avatar from _building blocks_
+ Chat as pop-up with avatar and message for people on-screen and.
 
## Hand-to-Hand combat

+ Disarm
+ Counter
+ Grab/Chokehold (PvP: buttonbash to break neck/free)

-----------------------
   
## World

A vehicle is required to get to another city by road. Traveling to another city the camera angle changes and it is like a highway racing game during which you'll see the environment change in accordance with the climate and terrain and cultural region.

### Money `[ ϕ ]`

The world of Art merge from a single currency, the ram, denoted with the symbol **ϕ**. The beginning of time is referred to as the "Passing of the Bill".
Some values have their own names:

Value | Name
---|---
ϕ1,- | Buck
ϕ10, | Croc
ϕ20, | Canary
ϕ50, | Flamingo


### Overview

![Home](Globe.png)

![Map](WorldMap.png)

### Cities/Regions

#### Western

Name | Real world | Defining | Language |
---|---|---|---
[**Grunstatt**](#Grunstatt) | Dutch/German/Danish | Hemp, sugar factory | Friets (Frisian/Dutch/German)
**Ånslo** | Norwegian/Swedish/Finnish | Fishballs
**Noopolis** | Italian/Greek/French | Cheese, wine and philosophy | Latin
**Airstrip 1** | British/American | Telescreens | New Speak

##### Grunstatt

Named after Groningen (Grunn in local dialect) with the layout of Grünstadt (after name) and look and feel of Ludwigshafen am Rhein.
> **Districts:** Rapperswinkel, ...

##### Noopolis
A mix mainly between Rome and Vienna, on the shore of the Quasea.

#### Southern

Name | Real world | Defining | Language |
---|---|---|---
**Zombuntu** (Savannah) | Central & South Africa | Wildlife and witchdoctors |  Zulu/Eritrean
**Agrobar** (Desert) | Nothern Africa | Oil | Arabic
**Axllotus** (Jungle) | Central & South America| Natives | Hindi
**Ichell** (Steppe) | Persia/Horn of Africa/Sofia | Goatboy | Sumerian



#### Eastern

* **Pingrad** (Russian/North Korean) | Communism
* **Balkan** (Eastern European) | 
* **Monkong** (Chinese/Tibetan/Japanese) | Pandas, robots and monks
* **Sloompur** (Australian/Indonesian/New Zealand) | Giant squids, small islands

### Vehicles

* Land
   - Cars
   - Motorcycles
   - Tanks
   - Boards

* Water
   - Rowboats
   - Speedboats
   - Jet-skis

### Weapons

##### Light weapons

+ Pistol: _Little Elvis_
+ Laser pistol: _Pincher_
+ SMG: _Spitfire_
+ Gyrojet: _Fracker_
+ Molotov cocktail


##### Heavy weapons

Slows you down somewhat, only one allowed.

+ Mini-gun: _Herzstark_
+ Flamethrower: _Libertorch_
+ Rocket Launcher: _Deathfist_
+ Explosive Drones: _Freedom Delivery System_
+ Electric arc: _Shocker_ (chains, revives fish and powers/disturbs electric lights)
 
### Gangs

For a list of gangs see [Gangs.md](Gangs.md)

Each will include:

+ Emblem and colors (applied to characters banners, vehicles)
+ Voices for both sexes
+ Accessories for character customization
+ Building(s)

### Shops

- **Bonbon Ted** - chocolatier
- **Fresh Meat** - butcher
- **Them Apples** - grocery store
- **Big Cheeze** - cheese store
- **Hot Buns** - bakery

#### Mobile

- **Fish van** (Western) "Fishheads" honk
- **Lumpia trike** (Eastern) "bell" honk
- **Couche couche train** (Southern) "Couche" honk

### Inspiration

- GTA2
- Postal
- The Matrix
- Illuminatus Trilogy
