/* OG Tatt
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef EXPLOSION_H
#define EXPLOSION_H

#include "effect.h"

class Explosion: public Effect
{
    friend class Enemy;
    DRY_OBJECT(Explosion, Effect);

public:
    Explosion(Context *context);
    static void RegisterObject(Context* context);

    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;
    void Start() override;
    void Set(const Vector3& position) override;

protected:
    SharedPtr<Light> light_;

private:
    SharedPtr<Sound> explode_sfx;
    float initialMass_;
    float initialBrightness_;
};

#endif
