/* OG Tatt
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ogtattcam.h"
#include "player.h"
#include "vehicle.h"

#include "inputmaster.h"

using namespace LucKey;

InputMaster::InputMaster(Context* context): Object(context)
{
    INPUT->SetMouseMode(MM_RELATIVE);

    keyBindingsMaster_[KEY_UP]     = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_UP]     = MasterAction::UP;
    keyBindingsMaster_[KEY_DOWN]   = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_DOWN]   = MasterAction::DOWN;
    keyBindingsMaster_[KEY_LEFT]   = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_LEFT]   = MasterAction::LEFT;
    keyBindingsMaster_[KEY_RIGHT]  = buttonBindingsMaster_[CONTROLLER_BUTTON_DPAD_RIGHT]  = MasterAction::RIGHT;
    keyBindingsMaster_[KEY_RETURN] = buttonBindingsMaster_[CONTROLLER_BUTTON_A]           = MasterAction::CONFIRM;
    keyBindingsMaster_[KEY_ESCAPE] = buttonBindingsMaster_[CONTROLLER_BUTTON_B]           = MasterAction::CANCEL;
    keyBindingsMaster_[KEY_P]      = buttonBindingsMaster_[CONTROLLER_BUTTON_START]       = MasterAction::PAUSE;
    keyBindingsMaster_[KEY_ESCAPE] = MasterAction::MENU;

    keyBindingsPlayer_[1][KEY_W]      = PlayerAction::MOVE_UP;
    keyBindingsPlayer_[1][KEY_S]      = PlayerAction::MOVE_DOWN;
    keyBindingsPlayer_[1][KEY_A]      = PlayerAction::MOVE_LEFT;
    keyBindingsPlayer_[1][KEY_D]      = PlayerAction::MOVE_RIGHT;
    keyBindingsPlayer_[1][KEY_Z]      = PlayerAction::MOVE_UP;   // AZERTY
    keyBindingsPlayer_[1][KEY_Q]      = PlayerAction::MOVE_LEFT; // AZERTY
    keyBindingsPlayer_[1][KEY_LSHIFT] = PlayerAction::ACTION0;
    keyBindingsPlayer_[1][KEY_SPACE]  = PlayerAction::ACTION1;
    keyBindingsPlayer_[1][KEY_RETURN] = PlayerAction::ACTION2;
    keyBindingsPlayer_[1][KEY_TAB]    = PlayerAction::ACTION3;
    keyBindingsPlayer_[1][KEY_KP_8]   = PlayerAction::FIRE_N;
    keyBindingsPlayer_[1][KEY_KP_5]   = PlayerAction::FIRE_S;
    keyBindingsPlayer_[1][KEY_KP_2]   = PlayerAction::FIRE_S;
    keyBindingsPlayer_[1][KEY_KP_4]   = PlayerAction::FIRE_W;
    keyBindingsPlayer_[1][KEY_KP_6]   = PlayerAction::FIRE_E;
    keyBindingsPlayer_[1][KEY_KP_9]   = PlayerAction::FIRE_NE;
    keyBindingsPlayer_[1][KEY_KP_3]   = PlayerAction::FIRE_SE;
    keyBindingsPlayer_[1][KEY_KP_1]   = PlayerAction::FIRE_SW;
    keyBindingsPlayer_[1][KEY_KP_7]   = PlayerAction::FIRE_NW;
    keyBindingsPlayer_[1][KEY_I]      = PlayerAction::FIRE_N;
    keyBindingsPlayer_[1][KEY_K]      = PlayerAction::FIRE_S;
    keyBindingsPlayer_[1][KEY_J]      = PlayerAction::FIRE_W;
    keyBindingsPlayer_[1][KEY_L]      = PlayerAction::FIRE_E;

    keyBindingsPlayer_[2][KEY_UP]     = PlayerAction::MOVE_UP;
    keyBindingsPlayer_[2][KEY_DOWN]   = PlayerAction::MOVE_DOWN;
    keyBindingsPlayer_[2][KEY_LEFT]   = PlayerAction::MOVE_LEFT;
    keyBindingsPlayer_[2][KEY_RIGHT]  = PlayerAction::MOVE_RIGHT;
    keyBindingsPlayer_[2][KEY_RSHIFT] = PlayerAction::ACTION0;

    for (int p{1}; p <= 4; ++p)
    {
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_A] = PlayerAction::ACTION0; //Run / Nitro
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_B] = PlayerAction::ACTION1; //Jump / Handbrake
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_X] = PlayerAction::ACTION2; //Enter/Leave vehicle
        buttonBindingsPlayer_[p][CONTROLLER_BUTTON_Y] = PlayerAction::ACTION3; //Burp/Fart/Horn
    }

    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(InputMaster, HandleKeyDown));
    SubscribeToEvent(E_KEYUP, DRY_HANDLER(InputMaster, HandleKeyUp));
    SubscribeToEvent(E_JOYSTICKBUTTONDOWN, DRY_HANDLER(InputMaster, HandleJoystickButtonDown));
    SubscribeToEvent(E_JOYSTICKBUTTONUP, DRY_HANDLER(InputMaster, HandleJoystickButtonUp));
    SubscribeToEvent(E_JOYSTICKAXISMOVE, DRY_HANDLER(InputMaster, HandleJoystickAxisMove));
    SubscribeToEvent(E_MOUSEMOVE, DRY_HANDLER(InputMaster, HandleMouseMove));
    SubscribeToEvent(E_INPUTBEGIN, DRY_HANDLER(InputMaster, HandleInputBegin));
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(InputMaster, HandleUpdate));
}

void InputMaster::HandleUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{   
    InputActions activeActions{};
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        Vector<PlayerAction> emptyActions{};
        activeActions.player_[pId] = emptyActions;
    }

    //Convert key presses to actions
    for (int key: pressedKeys_)
    {
        //Check for master key presses
        if (keyBindingsMaster_.Contains(key))
        {
            MasterAction action{ keyBindingsMaster_[key] };
            if (!activeActions.master_.Contains(action))
                activeActions.master_.Push(action);
        }
        //Check for player key presses
        for (Player* p: MC->GetPlayers())
        {

            int pId{ p->GetPlayerId() };
            if (keyBindingsPlayer_[pId].Contains(key))
            {
                PlayerAction action{ keyBindingsPlayer_[pId][key] };
                if (!activeActions.player_[pId].Contains(action))
                    activeActions.player_[pId].Push(action);
            }
        }
    }

    //Check for joystick button presses
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        int jId{ pId - 1 };

        for (int button: pressedJoystickButtons_[jId])
        {
            if (buttonBindingsPlayer_[pId].Contains(button))
            {
                PlayerAction action{ buttonBindingsPlayer_[pId][button]};
                if (!activeActions.player_[pId].Contains(action))
                    activeActions.player_[pId].Push(action);
            }
        }
    }

    //Handle the registered actions
    HandleActions(activeActions);
}

void InputMaster::HandleActions(const InputActions& actions)
{
    //Handle master actions
    for (MasterAction action: actions.master_)
    {
        switch (action)
        {
        case MasterAction::UP:                 break;
        case MasterAction::DOWN:               break;
        case MasterAction::LEFT:               break;
        case MasterAction::RIGHT:              break;
        case MasterAction::CONFIRM:            break;
        case MasterAction::CANCEL:             break;
        case MasterAction::PAUSE:              break;
        case MasterAction::MENU:               break;
        default: break;
        }
    }

    //Handle player actions
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        int jId{ pId - 1 };
        auto playerInputActions = actions.player_[pId];

        Controllable* controlled{ controlledByPlayer_[pId] };
        if (controlled)
        {
            Vector3 actionMove{ GetMoveFromActions(playerInputActions) };
            Vector3 stickMove{ leftStickPosition_[jId].x_, 0.f, -leftStickPosition_[jId].y_ };

            if (controlled->IsInstanceOf<Vehicle>())
                stickMove = Vector3(leftStickPosition_[jId].x_, 0.f, triggerPositions_[jId].y_ - triggerPositions_[jId].x_);

            Vector3 stickAim{ rightStickPosition_[jId].x_, 0.f, -rightStickPosition_[jId].y_ };

            controlled->SetMove(actionMove + stickMove);
            controlled->SetAim(GetAimFromActions(playerInputActions) + stickAim);

            std::bitset<4> restActions{};
            for (int a{ 0 }; a < static_cast<int>(PlayerAction::RESTACTIONS); ++a)
                restActions[a] = playerInputActions->Contains(static_cast<PlayerAction>(a));

            controlled->SetActions(restActions);
        }
    }
}

void InputMaster::Screenshot()
{
    Image screenshot(context_);
    Graphics* graphics{ GetSubsystem<Graphics>() };
    graphics->TakeScreenShot(screenshot);
    //Here we save in the Data folder with date and time appended
    String fileName{ GetSubsystem<FileSystem>()->GetProgramDir() + "Screenshots/Screenshot_" +
            Time::GetTimeStamp().Replaced(':', '_').Replaced('.', '_').Replaced(' ', '_')+".png" };
    //Log::Write(1, fileName);
    screenshot.SavePNG(fileName);
}

void InputMaster::HandleKeyDown(StringHash /*eventType*/, VariantMap &eventData)
{
    int key{ eventData[KeyDown::P_KEY].GetInt() };
    if (!pressedKeys_.Contains(key))
        pressedKeys_.Push(key);

    switch (key){
    case KEY_ESCAPE:{
        MC->Exit();
    } break;
    case KEY_9:{
        Screenshot();
    } break;
    default: break;
    }
}

void InputMaster::HandleKeyUp(StringHash /*eventType*/, VariantMap &eventData)
{
    int key{ eventData[KeyUp::P_KEY].GetInt() };
    if (pressedKeys_.Contains(key))
        pressedKeys_.Remove(key);
}

void InputMaster::HandleJoystickButtonDown(StringHash eventType, VariantMap &eventData)
{ (void)eventType;

    int jId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    ControllerButton button{ static_cast<ControllerButton>(eventData[JoystickButtonDown::P_BUTTON].GetInt()) };
    if (!pressedJoystickButtons_[jId].Contains(button))
        pressedJoystickButtons_[jId].Push(button);
}
void InputMaster::HandleJoystickButtonUp(StringHash eventType, VariantMap &eventData)
{ (void)eventType;

    int jId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    ControllerButton button{ static_cast<ControllerButton>(eventData[JoystickButtonUp::P_BUTTON].GetInt()) };
    if (pressedJoystickButtons_[jId].Contains(button))
        pressedJoystickButtons_[jId].Remove(button);
}

void InputMaster::HandleJoystickAxisMove(StringHash /*eventType*/, VariantMap& eventData)
{
    unsigned jId  { eventData[JoystickAxisMove::P_JOYSTICKID].GetUInt() };
    unsigned axis { eventData[JoystickAxisMove::P_AXIS].GetUInt() };
    float position{ eventData[JoystickAxisMove::P_POSITION].GetFloat() };

    switch (axis)
    {
    case 0u: leftStickPosition_ [jId].x_ = position; break;
    case 1u: leftStickPosition_ [jId].y_ = position; break;
    case 2u: rightStickPosition_[jId].x_ = position; break;
    case 3u: rightStickPosition_[jId].y_ = position; break;
    }
}

void InputMaster::HandleMouseMove(StringHash /*eventType*/, VariantMap& eventData)
{
    //Mouse aim
    if (INPUT->GetNumJoysticks() == 0u)
    {
        const IntVector2 mm{ eventData[MouseMove::P_DX].GetInt(),
                             eventData[MouseMove::P_DY].GetInt() };

        if (mm.Length() > 1.7f)
        {
            const Vector2 mouseDirection{ Vector2{ mm }.Normalized() };
            rightStickPosition_[0] = rightStickPosition_[0].Lerp(mouseDirection, .23f);
        }
    }
}

void InputMaster::HandleInputBegin(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    //Reset mouse aim
    if (INPUT->GetNumJoysticks() == 0u && INPUT->GetMouseMove() == IntVector2::ZERO)
        rightStickPosition_[0] = Vector2::ZERO;
}

Vector3 InputMaster::GetMoveFromActions(Vector<PlayerAction>* actions)
{
    return Vector3{Vector3::RIGHT *
                (actions->Contains(PlayerAction::MOVE_RIGHT) -
                 actions->Contains(PlayerAction::MOVE_LEFT))

                + Vector3::FORWARD *
                (actions->Contains(PlayerAction::MOVE_UP) -
                 actions->Contains(PlayerAction::MOVE_DOWN))};
}

Vector3 InputMaster::GetAimFromActions(Vector<PlayerAction>* actions)
{
    return Vector3{ Vector3::RIGHT *
                (actions->Contains(PlayerAction::FIRE_E) -
                 actions->Contains(PlayerAction::FIRE_W))

                + Vector3::FORWARD *
                (actions->Contains(PlayerAction::FIRE_N) -
                 actions->Contains(PlayerAction::FIRE_S))
                + Quaternion(45.f, Vector3::UP) * (Vector3::RIGHT *
                (actions->Contains(PlayerAction::FIRE_SE) -
                 actions->Contains(PlayerAction::FIRE_NW))

                + Vector3::FORWARD *
                (actions->Contains(PlayerAction::FIRE_NE) -
                 actions->Contains(PlayerAction::FIRE_SW)))};
}

void InputMaster::SetPlayerControl(Player* player, Controllable* controllable)
{
    int playerId{ player->GetPlayerId() };

    if (controlledByPlayer_.Contains(playerId))
    {
        if (controlledByPlayer_[playerId] == controllable)
            return;

        if (controlledByPlayer_[playerId] != nullptr)
            controlledByPlayer_[playerId]->ClearControl();
    }

    if (controllable)
        controllable->ClearControl();

    controlledByPlayer_[playerId] = controllable;
}

Player* InputMaster::GetPlayerByControllable(Controllable* controllable)
{
    for (int k: controlledByPlayer_.Keys())
    {
        if (controlledByPlayer_[k] == controllable)
            return MC->GetPlayer(k);
    }

    return nullptr;
}

Controllable* InputMaster::GetControllableByPlayer(int playerId)
{
    return controlledByPlayer_[playerId];
}
