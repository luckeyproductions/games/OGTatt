/* MIT License
//
// Copyright (c) 2024 LucKey Productions
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
*/

#include "witch.h"

using namespace Witch;

Vector<Rune> Wand::Plot(unsigned resolution, const Vector3& edgeNormal)
{
    if (resolution == 0)
        return { first_, second_ };

    Vector<Rune> curve{};
    unsigned numVerts{ resolution + 2u };

    for (unsigned i{ 0 }; i < numVerts; ++i)
    {
        float t{ i * 1.f / (numVerts - 1) };
        curve.Push(Plot(t, edgeNormal));
    }

    return curve;
}

Rune Wand::Plot(float t, const Vector3& edgeNormal)
{
    if (t <= 0.f)
        return anode();
    else if (t >= 1.f)
        return  cathode();

    PODVector<Vector3> segment{};
    for (int i{ 0 }; i < 3; ++i)
    {
        float dt{ (i == 0 ? 0.f : M_LARGE_EPSILON * (2 * (i - 1) - 1 )) };
        segment.Push(PlotPos(t + dt, edgeNormal));
    }

    Vector3 d{ (segment[2] - segment[1]).Normalized() };
    Vector3 vIn{ anode().Normal().ProjectOntoPlane(direction()).Normalized() };
    Vector3 vOut{ cathode().Normal().ProjectOntoPlane(direction()).Normalized() };

    if (vIn.LengthSquared() == 0.f)
        vIn = edgeNormal.ProjectOntoPlane(direction()).Normalized();
    if (vOut.LengthSquared() == 0.f)
        vOut = edgeNormal.ProjectOntoPlane(direction()).Normalized();

    Wand normWand{ { vIn, vIn }, { vOut, vOut }};
    Vector3 v{ normWand.PlotPos(t, edgeNormal).Normalized() };
    float slope{ v.DotProduct(d) };

    Vector3 normal{ v * cos(M_PI_2 * slope) - slope * direction() * sin(M_PI * Abs(slope))};
    normal = normal.Orthogonalize(d);

    float at{ Min(1.f, Max(0.f, 1.f - t * 1.f)) };
    float ct{ Min(1.f, Max(0.f, (t - .5f) * 1.f)) };
    at = at * at * at * at;
    ct = ct * ct * ct * ct;
    normal = normal.Lerp(anode().Normal(), at).Lerp(cathode().Normal(), ct);

    return { segment[0], normal.Normalized() };
}

Vector3 Wand::PlotPos(float t, const Vector3& edgeNormal)
{
    t = Clamp(t, 0.f, 1.f);
    float sinPiT{ sin(M_PI * t) };
    float cosPiT{ cos(M_PI * t) };

    float in{ -direction().DotProduct(anode()  .Normal()) };
    float out{ direction().DotProduct(cathode().Normal()) };
    float th{ Lerp(in, out, t) };

    Vector3 vIn{ anode().Normal().ProjectOntoPlane(direction()).Normalized() };
    Vector3 vOut{ cathode().Normal().ProjectOntoPlane(direction()).Normalized() };

    if (vIn.LengthSquared() == 0.f)
        vIn = edgeNormal;
    if (vOut.LengthSquared() == 0.f)
        vOut = edgeNormal;

    Vector3 v{ Quaternion::IDENTITY.Slerp({ vIn, vOut }, t) * vIn };
    Vector3 lateral{ Lerp(Vector3::ZERO, v * sinPiT / 2, th) };
    float z{ Lerp(t, (1 - cosPiT) / 2, th * th) };
    float m{ broom().Length() };

    float att{ static_cast<float>(atan(sqrt(cos(abs(th) * M_PI_4)) / M_PI_2)) };
    Vector3 delta{ m * (att * lateral + direction() * z) };

    return anode().Position() + delta;
}

Spell Sect::runes()
{
    Spell res{};

    for (auto s: *this)
        res.Push(s);

    return res;
}

Form Spell::Cast(unsigned resolution, const Matrix3x4& transform) const
{
    if (invoke_ == TRI)
        return TriCast(resolution, transform);
    if (invoke_ == QUAD)
        return QuadCast(resolution, transform);
    else
        return CurveCast(resolution, transform);
}

Form Spell::Cast(const IntVector2& resolution, const Matrix3x4& transform) const
{
    if (invoke_ == TRI)
        return TriCast(resolution.x_, transform);
    if (invoke_ == QUAD)
        return QuadCast(resolution, transform);
    else
        return CurveCast(resolution.x_, transform);
}

Form Spell::CurveCast(unsigned resolution, const Matrix3x4& transform) const
{
    Form form{ LINE_STRIP };
    PODVector<float>& vertexData{ form.vertexData_ };
    PODVector<unsigned short>& indexData{ form.indexData_ };

    int i{ 0 };
    for (unsigned r{ 0u }; r < Size(); ++r)
    {
        const Rune& rune{ At(r) };
        vertexData.Push(rune.Position().x_);
        vertexData.Push(rune.Position().y_);
        vertexData.Push(rune.Position().z_);
        vertexData.Push(rune.Normal().x_);
        vertexData.Push(rune.Normal().y_);
        vertexData.Push(rune.Normal().z_);

        indexData.Push(i++);
    }

    return form;
}

Form Spell::TriCast(unsigned resolution, const Matrix3x4& transform) const
{
    if (resolution < 0)
        resolution = 0;

    const Vector3 faceNormal{ FaceNormal() };
    Form form{ TRIANGLE_LIST };
    PODVector<float>& vertexData{ form.vertexData_ };
    PODVector<unsigned short>& indexData{ form.indexData_ };
    int i{ 0 };

    Wand highWand{ At(0), At(0) };

    for (unsigned s{ 0 }; s <= resolution; ++s)
    {
        const float t0{ (1.f + s) / (1.f + resolution) };
        Wand lowWand{ Wand{ At(0), At(1) }.Plot(t0, faceNormal), Wand{ At(0), At(2) }.Plot(t0, faceNormal) };

        for (unsigned h{ 0 }; h <= 2 * s; ++h)
        {
            bool inverted{ h % 2 != 0 };
            Rune v0, v1, v2;

            for (int v { 0 }; v < 3; ++v)
            {
                bool high{ (inverted ? v < 2 : v == 0)};
                float t1{ 0.f };

                if (!inverted)
                {
                    if (high)
                        t1 = .5f * h / Max(1, s);
                    else
                        t1 = (.5f * h + (v == 1)) / (1 + s);
                }
                else
                {
                    if (high)
                        t1 = (.5f * (h - 1) + (v == 1)) / s;
                    else
                        t1 = .5f * (h + 1) / (s + 1);
                }

                Rune vertex;

                if (high)
                    vertex = highWand.Plot(t1, faceNormal);
                else
                    vertex = lowWand.Plot(t1, faceNormal);

                if (transform != Matrix3x4::IDENTITY)
                    vertex = vertex.Transformed(transform);

                switch (v) {
                case 0: v0 = vertex; break;
                case 1: v1 = vertex; break;
                case 2: v2 = vertex; break;
                default: break;
                }
            }

            for (const Rune& r: { v0, v2, v1 })
            {
                vertexData.Push(r.Position().x_);
                vertexData.Push(r.Position().y_);
                vertexData.Push(r.Position().z_);
                vertexData.Push(r.Normal().x_);
                vertexData.Push(r.Normal().y_);
                vertexData.Push(r.Normal().z_);

                indexData.Push(i++);
            }
        }

        highWand = lowWand;
    }

    return form;
}

Form Spell::QuadCast(unsigned resolution, const Matrix3x4& transform) const
{
    return QuadCast({ static_cast<int>(resolution), static_cast<int>(resolution) }, transform);
}

Form Spell::QuadCast(const IntVector2& resolution, const Matrix3x4& transform) const
{
    const Vector3 faceNormal{ FaceNormal() };

    Vector3 averageVertexNormal{};
    for (int n{ 0 }; n < 4; ++n)
        averageVertexNormal += At(n).Normal();
    averageVertexNormal /= 4;
    bool flip{ faceNormal.DotProduct(averageVertexNormal) < 0.f };

    Form form{ TRIANGLE_LIST };
    PODVector<float>& vertexData{ form.vertexData_ };
    PODVector<unsigned short>& indexData{ form.indexData_ };
    int i{ 0 };

    Wand highWand{ At(0), At(1) };

    for (unsigned s{ 0 }; s <= resolution.x_; ++s)
    {
        const float t0{ (1.f + s) / (1.f + resolution.x_) };
        Wand lowWand{ Wand{ At(0), At(3) }.Plot(t0, faceNormal),
                      Wand{ At(1), At(2) }.Plot(t0, faceNormal) };

        for (unsigned h{ 0 }; h <= resolution.y_; ++h)
        {
            for (bool inverted: { false, true })
            {
                Rune v0, v1, v2;

                for (int v { 0 }; v < 3; ++v)
                {
                    float t1;

                    if (inverted)
                        t1 = (h + (v != 0)) / (resolution.y_ + 1.f);
                    else
                        t1 = (h + (v == 2)) / (resolution.y_ + 1.f);

                    bool high{ (inverted ? v != 1 : v == 0) };
                    Rune vertex;

                    if (high)
                        vertex = highWand.Plot(t1, faceNormal);
                    else
                        vertex = lowWand.Plot(t1, faceNormal);

                    if (transform != Matrix3x4::IDENTITY)
                        vertex = vertex.Transformed(transform);



                    switch (v)
                    { default: break;
                    case 0: v0 = vertex; break;
                    case 1: (flip ? v2 = vertex : v1 = vertex); break;
                    case 2: (flip ? v1 = vertex : v2 = vertex); break;
                    }
                }

                for (const Rune& r: { v0, v2, v1 })
                {
                    vertexData.Push(r.Position().x_);
                    vertexData.Push(r.Position().y_);
                    vertexData.Push(r.Position().z_);
                    vertexData.Push(r.Normal().x_);
                    vertexData.Push(r.Normal().y_);
                    vertexData.Push(r.Normal().z_);

                    indexData.Push(i++);
                }
            }
        }

        highWand = lowWand;
    }

    return form;
}

Vector3 Spell::FaceNormal(int i) const
{
    switch (invoke_)
    {
    case POINTS: case CURVE: default:
    {
        return Vector3::ZERO;
    }
    case TRI:
    {
        Vector3 edge1{ At(0 + i * 3).Position() - At(1 + i * 3).Position() };
        Vector3 edge2{ At(0 + i * 3).Position() - At(2 + i * 3).Position() };
        return edge1.CrossProduct(edge2).Normalized();
    }
    case QUAD:
    {
        const Vector3 edge1{ At(1 + i * 4).Position() - At(0 + i * 4).Position() };
        const Vector3 edge2{ At(3 + i * 4).Position() - At(0 + i * 4).Position() };
        const Vector3 edge3{ At(2 + i * 4).Position() - At(3 + i * 4).Position() };
        const Vector3 edge4{ At(2 + i * 4).Position() - At(1 + i * 4).Position() };
        const Vector3 windingNormal{ (edge1.CrossProduct(edge2).Normalized() + edge3.CrossProduct(edge4).Normalized()).Normalized() };

        return windingNormal;
    }
    }
}

