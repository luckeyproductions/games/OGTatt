/* MIT License
//
// Copyright (c) 2024 LucKey Productions
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
*/

#ifndef WITCH_H
#define WITCH_H

#include <initializer_list>

#include "../../luckey.h"

namespace Witch
{

struct Rune: public Pair<Vector3, Vector3>
{
    using Pair::Pair;

    Rune(const Vector3& position = Vector3::ZERO, const Vector3& normal = Vector3::UP): Pair<Vector3, Vector3>(position, normal.Normalized()) {}
    const Vector3& Position() const { return first_; }
    const Vector3& Normal() const { return second_; }

    Rune Transformed(const Matrix3x4& transform) const
    {
//        Vector3 scale{ transform.Scale() };
//        float scaleSign{ Sign(scale.x_) * Sign(scale.y_) * Sign(scale.z_)};

        return { transform * Position(),
                (transform.ToMatrix3() * Normal()).Normalized() };
    }
};

struct Wand: Pair<Rune, Rune>
{
    using Pair::Pair;

    Vector<Rune> Plot(unsigned resolution, const Vector3& edgeNormal = Vector3::ZERO);

    const Rune& anode() const { return first_ ; }
    const Rune& cathode() const { return second_ ; }
    Vector3 broom() const { return cathode().Position() - anode().Position(); }
    Vector3 direction() const { return broom().Normalized(); }

    Rune Plot(float t, const Vector3& edgeNormal = Vector3::ZERO);
    Vector3 PlotPos(float t, const Vector3& edgeNormal = Vector3::ZERO);
};

struct Form
{
    Form(PrimitiveType type):
        vertexData_{},
        indexData_{},
        type_{ type }
    {}

    PODVector<float> vertexData_{};
    PODVector<unsigned short> indexData_{};
    PrimitiveType type_;
};

class Spell: public Vector<Rune>
{
public:
    enum Invocation{ POINTS = 0, CURVE, TRI, QUAD, RING, SHEET };

    Spell(const std::initializer_list<Rune>& list): Spell(list, POINTS) {}
    Spell(const Vector<Rune>& runes = {}, Invocation type = POINTS): Vector<Rune>(runes),
        invoke_{ type }
    {}
    Invocation Type() const { return invoke_; }

    const Vector3& Position(int i) const { return At(Clamp(i, 0, static_cast<int>(Size()) - 1)).Position(); }
    const Vector3& Normal(int i)   const { return At(Clamp(i, 0, static_cast<int>(Size()) - 1)).Normal(); }
    Form Cast(unsigned resolution = 0, const Matrix3x4& transform = {}) const;
    Form Cast(const IntVector2& resolution = IntVector2::ZERO, const Matrix3x4& transform = {}) const;

    void Reverse()
    {
        Spell rev{};
        for (unsigned r{ Size() }; r != 0;)
            rev.Push(At(--r));
        Swap(rev);
    }

    Spell Transformed(Matrix3x4 transform) const
    {
        Spell res{ {}, Type() };
        for (const Rune& r: *this)
            res.Push(r.Transformed(transform));

        return res;
    }

private:
    Invocation invoke_;

    Form CurveCast(unsigned resolution = 0u,    const Matrix3x4& transform = {}) const;
    Form TriCast(  unsigned resolution = 0u,    const Matrix3x4& transform = {}) const;
    Form QuadCast( unsigned resolution = 0u,    const Matrix3x4& transform = {}) const;
    Form QuadCast(const IntVector2& resolution, const Matrix3x4& transform) const;

    Vector3 FaceNormal(int i = 0) const;
};

class Sect: public Vector<Spell>
{
    using Vector::Vector;
public:
    Spell runes();
};

}

#endif // WITCH_H
