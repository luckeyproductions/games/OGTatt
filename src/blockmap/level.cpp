/* OG Tatt
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <fstream>

#include "../wallcollider.h"
#include "../streetlight.h"
#include "../honti.h"
#include "../cookiejar.h"
#include "../frop.h"
#include "../ogtattcam.h"
#include "../spawnmaster.h"
#include "heightprofile.h"
#include "tile.h"

#include "level.h"

Level::Level(Context* context): Component(context),
    objects_{},
    rigidBody_{ nullptr },
    size_{}
{
//    SubscribeToEvent(E_UPDATE, DRY_HANDLER(Level, HandleUpdate));
//    rootNode_ = MC->world.scene->CreateChild("Level");

    context_->RegisterFactory<Tile>();

    SubscribeToEvent(E_UPDATE, DRY_HANDLER(Level, HandleUpdate));
}

void Level::OnNodeSet(Node* node)
{
    if (!node)
        return;

    rigidBody_ = node_->CreateComponent<RigidBody>();

    Node* groundNode{ node_->CreateChild("Ground") };
    groundNode->CreateComponent<RigidBody>();
    CollisionShape* groundCollider{ groundNode->CreateComponent<CollisionShape>() };

    Load("Maps/SmallTest.emp");
    groundCollider->SetBox({ size_.x_, 2.f, size_.z_ }, Vector3::DOWN);
}

void Level::Shift(const IntVector2& direction)
{
    for (Node* n: objects_ + node_)
    {
        if (Controllable* c{ n->GetDerivedComponent<Controllable>() })
            if (c->GetPlayer())
                continue;

        n->Translate({ direction.x_ * size_.x_,
                       0.f,
                       direction.y_ * size_.z_ },
                     TS_WORLD);
    }
}

void Level::HandleUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Vector3 cameraDelta{ MC->GetCamera(1)->GetNode()->GetWorldPosition() - node_->GetWorldPosition() };

    if (Abs(cameraDelta.x_) > size_.x_ * 1.5f)
        Shift({ RoundToInt(Sign(cameraDelta.x_)) * 3, 0 });
    if (Abs(cameraDelta.z_) > size_.z_ * 1.5f)
        Shift({ 0, RoundToInt(Sign(cameraDelta.z_)) * 3 });
}

bool Level::Load(const String& fileName)
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };

    XMLFile* mapXML{ cache->GetResource<XMLFile>(fileName) };

    if (!mapXML)
        return false;

    XMLElement mapElem{ mapXML->GetRoot("blockmap") };

    if (mapElem.IsNull())
        return false;

    const IntVector3 mapSize{ mapElem.GetIntVector3("map_size") };
    blockSize_ = mapElem.GetVector3("block_size");
    size_ = mapSize * blockSize_;

    node_->GetChild("Ground")->SetPosition(Vector3::DOWN * (mapSize.y_ + blockSize_.y_) / 2.0f);

    HashMap<int, HashMap<int, StaticModelGroup*>> blockSets;

    XMLElement blockSetRefElem{ mapElem.GetChild("blockset") };

    while (blockSetRefElem)
    {
        int blockSetId{ blockSetRefElem.GetInt("id") };
        String blockSetFileName{ blockSetRefElem.GetAttribute("name") };
        XMLFile* blockSetXML{ cache->GetResource<XMLFile>(blockSetFileName) };
        XMLElement blockSetElem{ blockSetXML->GetRoot("blockset") };
        XMLElement blockElem{ blockSetElem.GetChild("block") };

        while (blockElem)
        {
            String modelName{ blockElem.GetAttribute("model") };
            StaticModelGroup* blockGroup{ blockSets[blockSetId][blockElem.GetInt("id")] };

            if (!blockGroup)
            {
                blockGroup = node_->CreateComponent<StaticModelGroup>();
                blockGroup->SetModel(cache->GetResource<Model>(modelName));
                blockGroup->SetCastShadows(modelName.Contains("Wall"));

                if (!blockElem.GetAttribute("material").IsEmpty())
                {
                    blockGroup->SetMaterial(cache->GetResource<Material>(blockElem.GetAttribute("material")));
                }
                else
                {
                    XMLElement matElem{ blockElem.GetChild("material") };

                    while (matElem)
                    {
                        const unsigned index{ matElem.GetUInt("index") };
                        if (index < blockGroup->GetNumGeometries())
                            blockGroup->SetMaterial(index, cache->GetResource<Material>(matElem.GetAttribute("name")));

                        matElem = matElem.GetNext("material");
                    }
                }

                blockSets[blockSetId][blockElem.GetInt("id")] = blockGroup;
            }

            blockElem = blockElem.GetNext("block");
        }

        blockSetRefElem = blockSetRefElem.GetNext("blockset");
    }

    XMLElement layerElem{ mapElem.GetChild("gridlayer") };

    while (layerElem)
    {
        XMLElement blockElem{ layerElem.GetChild("gridblock") };

        while (blockElem)
        {
            IntVector3 coords{ blockElem.GetIntVector3("coords") };

            Node* blockNode{ node_->CreateChild("Block") };
            blockNode->SetPosition(Vector3((coords.x_ - mapSize.x_ / 2.0f) * blockSize_.x_,
                                           (coords.y_ - mapSize.y_ / 2.0f) * blockSize_.y_,
                                           (coords.z_ - mapSize.z_ / 2.0f) * blockSize_.z_));
            blockNode->SetRotation(blockElem.GetQuaternion("rot"));

            StaticModelGroup* blockGroup{ blockSets[blockElem.GetInt("set")][blockElem.GetInt("block")] };
            Model* blockModel{ (blockGroup ? blockGroup->GetModel() : nullptr) };
            String modelName{ (blockModel ? blockModel->GetName() : "") };

            if (modelName.Contains("StreetLight"))
            {
                blockNode->CreateComponent<StreetLight>();
                objects_.Push(blockNode);
            }
            else if (modelName.Contains("Cookiejar"))
            {
                blockNode->CreateComponent<Cookiejar>();
                objects_.Push(blockNode);
            }
            else if (modelName.Contains("Honti"))
            {
                blockNode->CreateComponent<Honti>();
                objects_.Push(blockNode);
            }
            else if (modelName.Contains("Frop"))
            {
                blockNode->CreateComponent<Frop>();
                objects_.Push(blockNode);
            }
            else if (blockElem.HasAttribute("elevation"))
            {
                const String elevationString{ blockElem.GetAttribute("elevation") };
                const String normalsString{ blockElem.GetAttribute("normals") };
                const String spanString{ blockElem.GetAttribute("span") };
                HeightProfile profile{ HeightProfile::fromString(elevationString, normalsString, spanString) };

                Material* mat{ nullptr };

                Tile* tile{ blockNode->CreateComponent<Tile>() };
                tile->Initialize(profile, mat);
            }
            else
            {
                blockGroup->AddInstanceNode(blockNode);
            }

            if (modelName.Contains("Wall")) {

                CollisionShape* collider{ node_->CreateComponent<CollisionShape>() };
                collider->SetTriangleMesh(blockGroup->GetModel(), 0, blockNode->GetWorldScale(), blockNode->GetWorldPosition(), blockNode->GetWorldRotation());
            }

            blockElem = blockElem.GetNext("gridblock");
        }

        layerElem = layerElem.GetNext("gridlayer");
    }

    for (Node* o: objects_)
        o->SetParent(o->GetScene());

    return true;
}

    /*
    TmxFile2D* tmxFile{ CACHE->GetResource<TmxFile2D>("Maps/smallTest.tmx") };
    if (tmxFile)
        InitializeFromMap(*tmxFile);
//    else
//        InitializeRandom();

}

void Level::InitializeFromMap(const TmxFile2D& tmxFile)
{
    float mapWidth{};
    float mapHeight{};

    int nthTileLayer{0};

    for (unsigned i{0}; i < tmxFile.GetNumLayers(); ++i) {

        const TmxLayer2D* layer{ tmxFile.GetLayer(i) };
        if (!layer)
            return;

        if (mapWidth < layer->GetWidth())
            mapWidth = layer->GetWidth();
        if (mapHeight < layer->GetHeight())
            mapHeight = layer->GetHeight();


        if (layer->GetType() == LT_TILE_LAYER){

            const TmxTileLayer2D* tileLayer{ static_cast<const TmxTileLayer2D*>(layer) };

            for (int y{0}; y < tileLayer->GetHeight(); ++y) {
                for (int x{0}; x < tileLayer->GetWidth(); ++x) {

                    Tile2D* tile{ tileLayer->GetTile(x, y) };

                    if (tile && tile->HasProperty("model")) {

                        TileInfo info{};
                        info.coords_ = IntVector3(x, nthTileLayer, y);
                        info.obstacle_ = tile->HasProperty("obstacle");

                        if (tile->HasProperty("model")) {

                            info.modelName_ = tile->GetProperty("model");
                        }

                        if (tile->HasProperty("materials")) {

                            String materials_s{ tile->GetProperty("materials") };
                            Vector<String> materialNames{ materials_s.Split(',') };

                            for (String& s: materialNames){
                                s = s.Trimmed();
                                if (!s.Length()){
                                    materialNames.Remove(s);
                                }
                            }
                            info.materialNames_ = materialNames;
                        }

                        AddTile(info);
                    }
                }
            }
            ++nthTileLayer;

        } else if (layer->GetType() == LT_OBJECT_GROUP){

            const TmxObjectGroup2D* objectGroup{ static_cast<const TmxObjectGroup2D*>(layer) };

            for (unsigned i{0}; i < objectGroup->GetNumObjects(); ++i) { ///Vector<TileMapObject2D*> TmxObjectGroup2D::GetObjects()

                TileMapObject2D* object{ objectGroup->GetObject(i) };

                int gid{ object->GetTileGid() };
                PropertySet2D* properties{ tmxFile.GetTilePropertySet(gid) };

                if (!properties)
                    continue;

                float scaleFactor{1.56f};
                Vector3 pos(scaleFactor * object->GetPosition().x_,
                            0.0f,
                            scaleFactor * object->GetPosition().y_);
                pos += Vector3(-0.5f*mapWidth, 0.0f, -0.5f*mapHeight+1);

                Quaternion rot{};   ///float TileMapObject2D::GetRotation()
                                    ///int TileMapObject2D::GetId()
                Vector2 size{object->GetSize()};

                //Create objects
                if (properties->HasProperty("streetlight")) {
                    SPAWN->Create<StreetLight>()->Set(pos);
                } else if (properties->HasProperty("car")) {
                    SPAWN->Create<Cookiejar>()->Set(pos);
                } else if (properties->HasProperty("bike")) {
                    SPAWN->Create<Honti>()->Set(pos);
                } else if (properties->HasProperty("frop")) {
                    SPAWN->Create<Frop>()->Set(pos);
                }
            }
        }
    }
    rootNode_->Translate(Vector3(-0.5f * mapWidth,
                                 0.0f,
                                 0.5f * mapHeight));
}

void Level::AddTile(const TileInfo& info)
{
//    tileMap_[info.coords_] = new Tile(info, this);
    new Tile(context_, info, this);
}

/*
void Level::InitializeRandom()
{

    IntVector2 firstCoordPair{IntVector2(0,0)};
    tileMap_[firstCoordPair] = new Tile(firstCoordPair, this);
    // Add random tiles
    int addedTiles{1};
    int worldSize{64};//Random(32, 128);

    while (addedTiles < worldSize){
        //Pick a random exsisting tile from a list.
        Vector<IntVector2> coordsVector{tileMap_.Keys()};
        IntVector2 randomTileCoords{coordsVector[Random((int)coordsVector.Size())]};

        //Check neighbours in random orer
        char startDir{static_cast<char>(Random(1,4))};
        for (int direction{startDir}; direction < startDir+4; direction++){
            int clampedDir{direction};
            if (clampedDir > 4) clampedDir -= 4;
            if (CheckEmptyNeighbour(randomTileCoords, (TileElement)clampedDir, true))
            {
                IntVector2 newTileCoords{GetNeighbourCoords(randomTileCoords, (TileElement)clampedDir)};
                AddTile(newTileCoords);
                addedTiles++;
            }
        }
    }

    AddColliders();
}

void Level::AddColliders()
{
   Vector<IntVector2> tileCoordsVec = tileMap_.Keys();
    for (uint nthTile = 0; nthTile < tileCoordsVec.Size(); nthTile++){
        for (int element = 0; element <= 4; element++){
            IntVector2 tileCoords{tileCoordsVec[nthTile]};
            IntVector2 checkCoords{GetNeighbourCoords(tileCoords, (TileElement)element)};
            if (CheckEmpty(checkCoords, false) && CheckEmpty(checkCoords, true)){
                collisionMap_[tileCoords] = new WallCollider(context_, this, tileCoords);
                break;
            }
        }
    }
}

bool Level::CheckEmpty(IntVector3 coords, bool checkTiles = true) const
{
    if (checkTiles) return (!tileMap_.Keys().Contains(coords));
    else return (!collisionMap_.Keys().Contains(coords));
}


bool Level::CheckEmptyNeighbour(IntVector3 coords, TileElement element, bool checkTiles = true) const
{
    return CheckEmpty(GetNeighbourCoords(coords, element), checkTiles);
}



IntVector3 Level::GetNeighbourCoords(IntVector3 coords, TileElement element) const
{
    IntVector3 shift = IntVector3::ZERO;
    switch (element){
    case TE_NORTH: shift.y_ =  1; break;
    case TE_EAST:  shift.x_ =  1; break;
    case TE_SOUTH: shift.y_ = -1; break;
    case TE_WEST:  shift.x_ = -1; break;
    case TE_NORTHEAST: shift.x_ =  1; shift.y_ =  1; break;
    case TE_SOUTHEAST: shift.x_ =  1; shift.y_ = -1; break;
    case TE_SOUTHWEST: shift.x_ = -1; shift.y_ = -1; break;
    case TE_NORTHWEST: shift.x_ = -1; shift.y_ =  1; break;
    default: case TE_CENTER: break;
    }
    return coords + shift;
}

CornerType Level::PickCornerType(IntVector3 tileCoords, TileElement element) const
{
    bool emptyCheck[3] = {false, false, false};
    switch (element){
    case TE_NORTHEAST: {
        emptyCheck[0] = CheckEmptyNeighbour(tileCoords, TE_NORTH);
        emptyCheck[1] = CheckEmptyNeighbour(tileCoords, TE_NORTHEAST);
        emptyCheck[2] = CheckEmptyNeighbour(tileCoords, TE_EAST);
    }
    break;
    case TE_SOUTHEAST: {
        emptyCheck[0] = CheckEmptyNeighbour(tileCoords, TE_EAST);
        emptyCheck[1] = CheckEmptyNeighbour(tileCoords, TE_SOUTHEAST);
        emptyCheck[2] = CheckEmptyNeighbour(tileCoords, TE_SOUTH);
    }break;
    case TE_SOUTHWEST: {
        emptyCheck[0] = CheckEmptyNeighbour(tileCoords, TE_SOUTH);
        emptyCheck[1] = CheckEmptyNeighbour(tileCoords, TE_SOUTHWEST);
        emptyCheck[2] = CheckEmptyNeighbour(tileCoords, TE_WEST);
    }break;
    case TE_NORTHWEST: {
        emptyCheck[0] = CheckEmptyNeighbour(tileCoords, TE_WEST);
        emptyCheck[1] = CheckEmptyNeighbour(tileCoords, TE_NORTHWEST);
        emptyCheck[2] = CheckEmptyNeighbour(tileCoords, TE_NORTH);
    }break;
    default: break;
    }

    int neighbourMask = 0;
    for (int i = 2; i >= 0; i--){
        neighbourMask += !emptyCheck[i] << i;
    }
    switch (neighbourMask){
    case 0: return CT_IN; break;
    case 1: return CT_TWEEN; break;
    case 2: return CT_DOUBLE; break;
    case 3: return CT_NONE; break;
    case 4: return CT_NONE; break;
    case 5: return CT_OUT; break;
    case 6: return CT_NONE; break;
    case 7: return (element == TE_SOUTHEAST) ? CT_FILL : CT_NONE; break;
    default: return CT_NONE; break;
    }
}
*/
