/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PLAYER_H
#define PLAYER_H

#include "mastercontrol.h"

class Controllable;
class Character;

class Player: public Object
{
    DRY_OBJECT(Player, Object);
public:
    Player(int playerId, Context* context);

    Vector3 GetPosition();
    Controllable* GetControllable();

    int GetPlayerId() const { return playerId_; }
    void AddScore(int points);
    unsigned GetScore() const { return balance_; }
    unsigned GetFlightScore() const { return flightScore_; }
    void Die();
    void Respawn();
    void ResetScore();
    void Pay(int bucks, bool cash = true);

    void SetCharacter(Character* character) { character_ = character; }
    Character* GetCharacter() const { return character_; }

    bool IsAlive() const noexcept { return alive_; }
    bool IsHuman() const noexcept { return !autoPilot_; }
    void EnterLobby();
    void EnterPlay();

private:
    int playerId_;
    bool autoPilot_;
    bool alive_;

    Character* character_;

    int balance_;
    unsigned wallet_;
    unsigned flightScore_;
    float interest_;

    void SetBalance(int bucks);
    void SetWallet(unsigned bucks);
};

#endif // PLAYER_H
